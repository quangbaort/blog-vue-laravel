<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link href="https://fonts.googleapis.com/css2?family=Sriracha&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Sriracha', cursive;
            }
        </style>
        <script src="{{asset('js/app.js')}}"></script>
    </head>
    <body class="antialiased">
        <div id="lionel">
            <router-view></router-view>
        </lionel>
    </body>
</html>
