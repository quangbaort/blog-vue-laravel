import Vue from 'vue';
import VueRouter from 'vue-router'
import HomePage from './pages/Home'
import NotFound from './pages/NotFound'
import About from './pages/About'
Vue.use(VueRouter);
export default new VueRouter({
    mode: 'history',
    routes : [
        {path : '/' , name : 'home' , component : HomePage},
        {path : '/about' , name : 'about' , component : About},
        {path : '*' , name : 'notFound' , component : NotFound}
    ]
})